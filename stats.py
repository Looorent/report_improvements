import datetime
import load_work_themes

# Mise en place du test
print("Which action?")
	# Input Datas
print("1 => Write Datas")
	# Check Datas
print("2 => Read Datas")
	# Add theme
print("3 => Add Working Theme")
value = False
print("Choose a number: ")
while not value:
	result = input()
	try:
		result = int(result)
	except ValueError as ve:
		print("Choose a correct value: 1,2 or 3")
	else: 
		if result in [1, 2, 3]:
			value = True
		else:
			print("Choose a correct number: 1,2 or 3")
value = False
if result == 1:
	while not value:
		print("Choose a subject : ")
		for key, value in load_work_themes.extract_themes_value().items():
			print("{0} => {1}".format(value, key))
		subject = input()
		try:
			subject = int(subject)
		except ValueError as ve:
			print("Choose a correct value: 1,2 or 3")
		else: 
			if subject in load_work_themes.extract_themes_value().keys():
				current_subject = load_work_themes.extract_themes_value().get(subject)
				current_date = datetime.datetime.today().strftime('%d-%m-%Y')
				line = current_date + ',' + current_subject + ',' + '\n'
				with open('datas.csv', 'a') as file:
					file.write(line)
			else:
				print("Choose a correct number: 1,2 or 3")
if result == 2:
	print("DATAS GLOBAL REPORT")
	global_count = 0
	subjects_count = load_work_themes.themes_count_init()
	with open('datas.csv', 'r') as file:
		for line in file:
			global_count += 1
			local_count = subjects_count.get(line.split(',')[1])
			local_count += 1
			subjects_count[line.split(',')[1]] = local_count
	for key, value in subjects_count.items():
		print("{0}: {1}%".format(key, round((value/global_count*100),1)))
if result == 3:
	print("------ New Theme ------")
	name = input("Get name theme:")
	load_work_themes.add_theme(name)
