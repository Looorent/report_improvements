
path_themes_file = "themes.csv"


def set_file_path(new_path_to_file):
    global path_themes_file
    path_themes_file = new_path_to_file


def extract_themes_value():
    all_themes = {}
    with open(path_themes_file, 'r') as file:
        for line in file:
            list_line = line.split(',')
            key, value = int(list_line[0]), list_line[1]
            all_themes[key] = value
    return all_themes


def themes_count_init():
    themes_count_item = {}
    with open(path_themes_file, 'r') as file:
        for line in file:
            name = line.split(',')[1]
            themes_count_item[name] = 0
    return themes_count_item


def get_lenght_themes_list():
    return len(extract_themes_value())


def add_theme(theme_name):
    new_theme_index = get_lenght_themes_list() + 1
    line = str(new_theme_index) + ',' + theme_name + ',' + '\n'
    with open(path_themes_file, 'a') as file:
        file.write(line)
